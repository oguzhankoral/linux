set terminal png
set output "result.png"
set title "Volume over optimization Steps"
set xlabel "Step"
set ylabel "Volume"
plot "result" with lines
