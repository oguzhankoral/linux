#!/bin/bash
# Author	: Oguzhan Koral
# Written Date	: 17.04.2020 - 27.04.2020
# Purpose	: Homework-1 of HBM801 Lecture

filename="$1"			#input first file
filename2="$2"			#input second file
filename3="$3"			#input third file

### 1st requirement
for i in $1 $2 $3
do
	grep -i "enthalpy new" $i > $i.txt
	cut -d" " -f 23 $i.txt > result
	gnuplot plotter.plot
	cp result.png $i.1.png
	rm $i.txt result.png result
done

### 2nd requirement
for i in $1 $2 $3
do
        grep -i "new unit-cell volume" $i > $i.txt
        cut -d" " -f 12 $i.txt > result
        gnuplot plotter2.plot
        cp result.png $i.2.png
        rm $i.txt result.png result
done

### 3rd requirement
touch report2.txt
echo OUTPUT_NAME,ENERGY,ITER,VOL > report2.txt

for i in $1 $2 $3
do
        grep -i "enthalpy new" $i > $i.txt
	grep -i "new unit-cell volume" $i > $i.2.txt
        cut -d" " -f 23 $i.txt > result1
	cut -d" " -f 17 $i.2.txt > result2
	a=$(awk '{print $1}' result1 | tail -1)
	b=$(grep -c "new unit-cell volume" $i)
	c=$(awk '{print $1}' result2 | tail -1)
	echo "$i,$a,$b,$c" >> report2.txt
        rm $i.txt $i.2.txt result1 result.txt
done
clear
column report2.txt -t -s "," > report.txt
rm -r result2 report2.txt

###4th requirement
echo "Lowest Energy Structure:"
sort -k2nr report.txt | tail -1 >> result3.txt
d=$(awk '{print $1,$2}' result3.txt)
echo "$d Ry"
rm -r result3.txt

