Shell script which is data_visualization.sh works for data visualization and report.

## Quickstart
After opening terminal,
* `./data_visualization.sh output1.out output2.out output3.out` command run the script with arguments that related output files

## Results
* Report.txt file comprised from output files' final iteration
* Print value of minimum energy as Ry on screen

## The commands that used
* gnuplot (visualization)
* grep
* cut
* awk
* sort
* column