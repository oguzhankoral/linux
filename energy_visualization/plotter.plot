set terminal png
set output "result.png"
set title "Enthalphy Energy"
set xlabel "Step"
set ylabel "Energy"
plot "result" with lines
